﻿using Spectre.Console;

namespace Exceptions;
public static class ExceptionExtensions
{
    public static Markup ToDisplayError(this Exception ex)
    {
        return Markup.FromInterpolated($"[red]Error: {ex.Message}[/]");
    }
    public static Markup ToDisplayUserFriendlyError(this IOException ex)
    {
        return Markup.FromInterpolated($"[red]Error occured. Please ensure that DB is reachable[/]");
    }
    public static Markup ToDisplayUserFriendlyError(this Exception ex)
    {
        return Markup.FromInterpolated($"[red]{ex.Message}[/]");
    }
}