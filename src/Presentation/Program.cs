﻿using McMaster.Extensions.CommandLineUtils;
using Microsoft.Extensions.DependencyInjection;
using Spectre.Console;
using Presentation.Commands;
using Configuration;
using DataAccess.Data;
using Exceptions;

namespace Presentation;

[Command(Description = "Book list management system")]
[Subcommand(typeof(AddBookCommand))]
[Subcommand(typeof(AddAddressCommand))]
[Subcommand(typeof(DisplayBookListCommand))]
[Subcommand(typeof(DisplayAuthorListCommand))]
[Subcommand(typeof(UpdateBookCommand))]
[Subcommand(typeof(DeleteBookCommand))]
[HelpOption]
public class Program
{
    static void Main(string[] args)
    {
        var services = new ServiceCollection().RegisterServices();
        var serviceProvider = services.BuildServiceProvider();

        using (var scope = serviceProvider.CreateScope())
        {
            var dbContext = scope.ServiceProvider.GetRequiredService<LibraryDbContext>();
        }

        var app = new CommandLineApplication<Program>();
        app.Conventions
            .UseDefaultConventions()
            .UseConstructorInjection(serviceProvider);
        try
        {
            app.Execute(args);
        }
        catch (Exception ex)
        {
            AnsiConsole.Write(ex.ToDisplayError());
        }
    }
}