﻿using BusinessLogic;
using McMaster.Extensions.CommandLineUtils;
using Spectre.Console;
using System.ComponentModel.DataAnnotations;
using Exceptions;
using DataAccess.Interfaces;

namespace Presentation.Commands;

[Command(Name = "newbook", Description = "Add information about some book")]
public class AddBookCommand
{
    private readonly ILibraryService _syncDataService;
    public AddBookCommand(ILibraryService addData)
    {
        _syncDataService = addData;
    }

    [Required, Option("-t|--title <TITLE>", "Enter the title of the book", CommandOptionType.SingleValue)]
    public string Title { get; set; }

    [Required, Option("-y|--year <YEAR>", "Enter the year of the book", CommandOptionType.SingleValue)]
    public int Year { get; set; }
    [Required, Option("-g|--genre <GENRE>", "Enter the genre of the book", CommandOptionType.SingleValue)]
    public string Genre { get; set; }
    [Required, Option("-fn|--firstName <FIRSTNAME>", "Enter the author's first name", CommandOptionType.SingleValue)]
    public string FirstName { get; set; }
    [Required, Option("-ln|--lastName <LASTNAME>", "Enter the author's last name", CommandOptionType.SingleValue)]
    public string LastName { get; set; }

    public async Task OnExecuteAsync()
    {
        try
        {
            await _syncDataService.RegisterNewBookAsync(Title, Year, Genre, FirstName, LastName);
            AnsiConsole.MarkupLine($"[green]Book saved successfully![/]");
        }
        catch (IOException ex)
        {
            AnsiConsole.Write(ex.ToDisplayUserFriendlyError());
        }
    }
}

