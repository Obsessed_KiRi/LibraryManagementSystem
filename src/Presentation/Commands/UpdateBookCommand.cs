﻿using BusinessLogic.Interfaces;
using DataAccess.Interfaces;
using McMaster.Extensions.CommandLineUtils;
using Spectre.Console;

namespace Presentation.Commands;

[Command(Name = "update", Description = "Update information about books, enter the title of the book that you want to update")]
public class UpdateBookCommand
{
    private readonly IBookRepository _bookRepo;
    private readonly IAuthorRepository _authorRepo;
    public UpdateBookCommand(IBookRepository bookRepo, IAuthorRepository authorRepo)
    {
        _bookRepo = bookRepo;
        _authorRepo = authorRepo;
    }

    [Option("-id <ID>", "Enter ID of the book you want to update", CommandOptionType.SingleValue)]
    public int Id { get; set; }
    [Option("-t|--title <TITLE>", "Enter new title of the book", CommandOptionType.SingleValue)]
    public string Title { get; set; }

    [Option("-y|--year <YEAR>", "Enter new year of the book", CommandOptionType.SingleValue)]
    public int Year { get; set; }
    [Option("-auid|--authorid <AUTHORID>", "Enter id of the author", CommandOptionType.SingleValue)]
    public int AuthorID { get; set; }

    public async Task OnExecuteAsync()
    {
        var bookInfo = await _bookRepo.GetBookByIdAsync(Id);
        if (bookInfo is null)
        {
            AnsiConsole.Markup("[red]Book is not found in database.. Maybe you should try another ID?[/]");
            return;
        }

        bookInfo.Title = Title;
        bookInfo.PublishYear = Year;

        if (bookInfo.Author.AuthorId != AuthorID)
        {
            var authorInfo = await _authorRepo.GetAuthorByIdAsync(AuthorID);
            if (authorInfo is null)
            {
                AnsiConsole.Markup("[red]Author is not found in the database.. Maybe you should try another author ID?[/]");
                return;
            }

            bookInfo.Author = authorInfo;
        }

        await _bookRepo.UpdateBookAsync(bookInfo);
        AnsiConsole.MarkupLine($"[green]Book updated successfully![/]");
    }
}
