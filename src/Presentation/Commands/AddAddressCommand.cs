﻿using BusinessLogic;
using DataAccess.Interfaces;
using Exceptions;
using McMaster.Extensions.CommandLineUtils;
using Spectre.Console;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Commands;

[Command(Name = "address", Description = "Add some information about author")]
public class AddAddressCommand
{
    private readonly ILibraryService _syncDataService;
    public AddAddressCommand(ILibraryService addData)
    {
        _syncDataService = addData;
    }

    [Required, Option("-id|--authorId <ID>", "Enter the author's ID", CommandOptionType.SingleValue)]
    public int AuthorId { get; set; }
    [Required, Option("-c|--country <COUNTRY>", "Enter the author's country", CommandOptionType.SingleValue)]
    public string Country { get; set; }

    [Required, Option("-a|--address <ADDRESS>", "Enter the author's address", CommandOptionType.SingleValue)]
    public string Address { get; set; }
    [Option("-pn|--phoneNumber <NUMBER_string>", "Enter the author's phone number", CommandOptionType.SingleValue)]
    public string PhoneNumber { get; set; }
    [Option("-m|--mail <MAIL>", "Enter the author's mail", CommandOptionType.SingleValue)]
    public string Mail { get; set; }
    public async Task OnExecuteAsync()
    {
        try
        {
            await _syncDataService.RegisterNewAddressAsync(AuthorId, Country, Address, PhoneNumber, Mail);
            AnsiConsole.MarkupLine($"[green]Information added successfully![/]");
        }
        catch (InvalidOperationException)
        {
            AnsiConsole.MarkupLine($"[red]Error occurred: Author with ID: '{AuthorId}' not found![/]");
        }
        catch (IOException ex)
        {
            AnsiConsole.Write(ex.ToDisplayUserFriendlyError());
        }
    }
}