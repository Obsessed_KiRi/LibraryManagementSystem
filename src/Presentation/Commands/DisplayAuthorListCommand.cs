﻿using McMaster.Extensions.CommandLineUtils;
using Presentation.Outputters;
using Spectre.Console;
using System.ComponentModel.DataAnnotations;
using DataAccess.Interfaces;
using System.Runtime.CompilerServices;

namespace Presentation.Commands;

[Command(Name = "author", Description = "Display information about authors and their books")]
public class DisplayAuthorListCommand
{
    private readonly IAuthorRepository _authorRepo;
    public DisplayAuthorListCommand(IAuthorRepository authorRepo)
    {
        _authorRepo = authorRepo;
    }
    [Option("-fn|--firstName <FIRSTNAME>", "Enter the author's first name", CommandOptionType.SingleValue)]
    public string FirstName { get; set; }
    [Option("-y|--year <YEAR>", "Enter the book's release date", CommandOptionType.SingleValue)]
    public int? Year { get; set; }
    [Option("-a|--address <ADDRESS>", "Enter the author's address", CommandOptionType.SingleValue)]
    public string Address { get; set; }
    [Option("-c|--country <COUNTRY>", "Enter the author's country", CommandOptionType.SingleValue)]
    public string Country { get; set; }
    public async Task OnExecuteAsync()
    {

        if (!string.IsNullOrEmpty(FirstName))
        {
            var data = await _authorRepo.GetAuthorsByNameAsync(FirstName);
            var table = data.ToDisplayAuthorInfo();
            AnsiConsole.Write(table);
        }
        else if (Year.HasValue)
        {
            var data = await _authorRepo.GetAuthorWithBookYearAsync(Year.Value);
            var table = data.ToDisplayAuthorWithBookYear();
            AnsiConsole.Write(table);
        }
        else if (!string.IsNullOrEmpty(Address) && (!string.IsNullOrEmpty(Country)))
        {
            var data = await _authorRepo.GetAuthorByAdressAndCountry(Address, Country);
            var table = data.ToDisplayAuthorWithAdress();
            AnsiConsole.Write(table);
        }
        else
        {
            var data = await _authorRepo.GetAuthorsAndBookNumbersAsync();
            var table = data.ToDisplayAuthorWithBookNumber();
            AnsiConsole.Write(table);
        }
    }
}