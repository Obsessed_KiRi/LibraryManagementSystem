﻿using McMaster.Extensions.CommandLineUtils;
using Presentation.Outputters;
using BusinessLogic.Interfaces;
using Spectre.Console;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Commands;

[Command(Name = "book", Description = "Display all information about books")]
[Subcommand(typeof(DisplayAuthorListCommand))]
public class DisplayBookListCommand
{
    private readonly IBookRepository _bookRepo;
    public DisplayBookListCommand(IBookRepository bookRepo)
    {
        _bookRepo = bookRepo;
    }
    [Option("-t|--title <TITLE>", "Display specific information about book by title", CommandOptionType.SingleValue)]
    public string Title { get; set; }
    [Option("-fn|--firstName <FIRSTNAME>", "Enter the author's first name", CommandOptionType.SingleValue)]
    public string FirstName { get; set; }
    [Option("-ln|--lastName <LASTNAME>", "Enter the author's last name", CommandOptionType.SingleValue)]
    public string LastName { get; set; }
    [Option("-c|--count", "Displays amount of registered books", CommandOptionType.NoValue)]
    public bool Count { get; set; }

    public async Task OnExecuteAsync()
    {
        if (Count)
        {
            var data = await _bookRepo.GetAmountOfBooksAsync();
            var table = data.ToDisplayAmountBooks();
            AnsiConsole.Write(table);
        }
        else if (!string.IsNullOrEmpty(Title))
        {
            var data = await _bookRepo.GetBookByTitleAsync(Title);
            var table = data.ToDisplayTable();
            AnsiConsole.Write(table);
        }
        else if (!string.IsNullOrEmpty(FirstName) && !string.IsNullOrEmpty(LastName))
        {
            var data = await _bookRepo.GetBookByAuthorAsync(FirstName, LastName);
            var table = data.ToDisplayBookInfo();
            AnsiConsole.Write(table);
        }
        else
        {
            var data = await _bookRepo.GetAllBookInfosAsync();
            var table = data.ToDisplayTable();
            AnsiConsole.Write(table);
        }
    }
}
