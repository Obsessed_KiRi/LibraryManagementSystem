﻿using BusinessLogic.Interfaces;
using McMaster.Extensions.CommandLineUtils;
using Spectre.Console;
using System.ComponentModel.DataAnnotations;

namespace Presentation.Commands;

[Command(Name = "delete", Description = "Delete information about book")]
public class DeleteBookCommand
{
    private readonly IBookRepository _bookRepo;
    public DeleteBookCommand(IBookRepository bookRepo)
    {
        _bookRepo = bookRepo;
    }
    [Required, Option("-id <ID>", "Enter the ID of the book", CommandOptionType.SingleValue)]
    public int Id { get; set; }

    public async Task OnExecuteAsync()
    {
        var book = await _bookRepo.GetBookByIdAsync(Id);

        if (book is null)
        {
            AnsiConsole.MarkupLine($"[red]Book with ID: {Id} not found![/]");
            return;
        }

        await _bookRepo.DeleteBook(book);
        AnsiConsole.MarkupLine($"[green]Book with ID: {Id} removed successfully![/]");
    }
}
