﻿using DataAccess.Data;
using Microsoft.Extensions.DependencyInjection;
using BusinessLogic;
using Presentation.Commands;
using Presentation.Outputters;
using BusinessLogic.Interfaces;
using DataAccess.Interfaces;

namespace Configuration;

public static class ServiceConfiguration
{
    public static IServiceCollection RegisterServices(this IServiceCollection services)
    {
        services.AddSingleton<LibraryDbContext>();
        services.AddTransient<ILibraryService, LibraryService>();
        services.AddSingleton<IBookRepository, BookRepository>();
        services.AddTransient<AddBookCommand>();
        services.AddTransient<IAuthorRepository, AuthorRepository>();

        return services;
    }
}