﻿using DataAccess.Entities;
using Spectre.Console;
using DataAccess.DTO;

namespace Presentation.Outputters;

public static class ConsoleOutputter
{
    public static Table ToDisplayTable(this List<Book> books)
    {
        var table = new Table().AddColumns("[red]Id[/]", "[red]Title[/]", "[red]Year[/]");
        foreach (var book in books)
        {
            table.AddRow(book.BookId.ToString(), book.Title, book.PublishYear.ToString());
        }
        return table;
    }

    public static Table ToDisplayTable(this Book book)
    {
        var table = new Table().AddColumns("[red]Id[/]", "[red]Title[/]", "[red]Year[/]");
        table.AddRow(book.BookId.ToString(), book.Title, book.PublishYear.ToString());
        return table;
    }

    public static Table ToDisplayBookInfo(this List<BookWithAuthorNameDTO> books)
    {
        var table = new Table().AddColumns("[red]Id[/]", "[red]Title[/]", "[red]Year[/]", "[red]Author[/]");
        foreach (var info in books)
        {
            table.AddRow(info.Id.ToString(), info.Title, info.Year.ToString(), info.AuthorName);
        }
        return table;
    }

    public static Table ToDisplayAuthorInfo(this List<Author> author)
    {
        var table = new Table().AddColumns("[red]Id[/]", "[red]Author's First Name[/]", "[red]Author's Last Name[/]");
        foreach (var info in author)
        {
            table.AddRow(info.AuthorId.ToString(), info.FirstName, info.LastName);
        }
        return table;
    }

    public static Table ToDisplayAuthorWithBookYear(this List<AuthorWithBookYearDTO> data)
    {
        var table = new Table().AddColumns("[red]Year[/]", "[red]Author's Full Name[/]");
        foreach (var info in data)
        {
            table.AddRow(info.PublishYear.ToString(), info.AuthorFullName);
        }
        return table;
    }

    public static Table ToDisplayAuthorWithBookNumber(this List<AuthorBookCountDTO> data)
    {
        var table = new Table().AddColumns("[red]Author's Full Name[/]", "[red]Number of Books[/]");
        foreach (var info in data)
        {
            table.AddRow(info.AuthorName, info.BookCount.ToString());
        }
        return table;
    }

    public static Table ToDisplayAmountBooks(this BookCountDTO count)
    {
        var table = new Table().AddColumn("[red]Amount of books in system[/]");
        table.AddRow(count.BookCount.ToString());
        return table;
    }

    public static Table ToDisplayAuthorWithAdress(this List<AuthorWithAddressDTO> data)
    {
        var table = new Table().AddColumns("[red]Author's name[/]", "[red]Author's address[/]");
        foreach (var info in data)
        {
            table.AddRow(info.AuthorFullName.ToString(), info.FullAddress);
        }
        return table;
    }

}
