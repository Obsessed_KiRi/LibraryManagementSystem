﻿using DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using DataAccess.Entities;
using BusinessLogic.Interfaces;
using DataAccess.Interfaces;
using Spectre.Console;

namespace BusinessLogic;

public class LibraryService : ILibraryService
{
    private readonly IBookRepository _bookRepository;
    private readonly IAuthorRepository _authorRepository;
    public LibraryService(IBookRepository bookRepository, IAuthorRepository authorRepository)
    {
        _bookRepository = bookRepository;
        _authorRepository = authorRepository;
    }

    public async Task RegisterNewBookAsync(string title, int year, string genre, string firstName, string lastName)
    {

        var existingAuthor = await _authorRepository.GetAuthorByFullNameAsync(firstName, lastName);

        if (existingAuthor is null)
        {
            existingAuthor = new Author
            {
                FirstName = firstName,
                LastName = lastName,
            };
        }
        var newBook = new Book
        {
            Title = title,
            PublishYear = year,
            Genre = genre,
            Author = existingAuthor
        };

        await _bookRepository.AddBookAsync(newBook);
    }
    public async Task RegisterNewAddressAsync(int id, string country, string address, string number, string mail)
    {
        var author = await _authorRepository.GetAuthorByIdAsync(id);

        if (author is null)
        {
            throw new InvalidOperationException();
        }

        author.Address = new Address
        {
            AddressName = address,
            Country = country,
        };

        author.ContactInfo = new ContactInfo
        {
            PhoneNumber = number,
            Mail = mail
        };

        await _authorRepository.UpdateAuthorInfoAsync(author);
    }
}