﻿using DataAccess.DTO;
using DataAccess.Entities;

namespace BusinessLogic.Interfaces;
public interface IBookRepository
{
    Task<Book> GetBookByTitleAsync(string title);
    Task <List<Book>> GetAllBookInfosAsync();
    Task AddBookAsync(Book book);
    Task DeleteBook(Book book);
    Task UpdateBookAsync(Book book);
    Task<Book> GetBookByIdAsync(int id);
    Task<List<BookWithAuthorNameDTO>> GetBookByAuthorAsync(string firstName, string lastName);
    Task<BookCountDTO> GetAmountOfBooksAsync();
}