﻿namespace DataAccess.Interfaces;

public interface ILibraryService
{
    Task RegisterNewBookAsync(string title, int year, string genre, string firstName, string lastName);
    Task RegisterNewAddressAsync(int id, string country, string adress, string number, string mail);
}
