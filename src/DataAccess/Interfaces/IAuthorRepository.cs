﻿using DataAccess.DTO;
using DataAccess.Entities;

namespace DataAccess.Interfaces;

public interface IAuthorRepository
{
    Task<Author> GetAuthorByIdAsync(int id);
    Task<Author> GetAuthorByFullNameAsync(string firstName, string lastName);
    Task<List<AuthorBookCountDTO>> GetAuthorsAndBookNumbersAsync();
    Task<List<AuthorWithBookYearDTO>> GetAuthorWithBookYearAsync(int year);
    Task<List<Author>> GetAuthorsByNameAsync(string firstName);
    Task UpdateAuthorInfoAsync(Author add);
    Task<List<AuthorWithAddressDTO>> GetAuthorByAdressAndCountry(string address, string country);
}
