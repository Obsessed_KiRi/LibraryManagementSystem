﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class NameChanges : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Authors_Adresses_AdressEntityId",
                table: "Authors");

            migrationBuilder.DropIndex(
                name: "IX_Authors_AdressEntityId",
                table: "Authors");

            migrationBuilder.RenameColumn(
                name: "AdressEntityId",
                table: "Authors",
                newName: "AddressEntityId");

            migrationBuilder.RenameColumn(
                name: "AdressName",
                table: "Adresses",
                newName: "AddressName");

            migrationBuilder.RenameColumn(
                name: "AdressId",
                table: "Adresses",
                newName: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Authors_AddressEntityId",
                table: "Authors",
                column: "AddressEntityId",
                unique: true,
                filter: "[AddressEntityId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Authors_Adresses_AddressEntityId",
                table: "Authors",
                column: "AddressEntityId",
                principalTable: "Adresses",
                principalColumn: "AddressId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Authors_Adresses_AddressEntityId",
                table: "Authors");

            migrationBuilder.DropIndex(
                name: "IX_Authors_AddressEntityId",
                table: "Authors");

            migrationBuilder.RenameColumn(
                name: "AddressEntityId",
                table: "Authors",
                newName: "AdressEntityId");

            migrationBuilder.RenameColumn(
                name: "AddressName",
                table: "Adresses",
                newName: "AdressName");

            migrationBuilder.RenameColumn(
                name: "AddressId",
                table: "Adresses",
                newName: "AdressId");

            migrationBuilder.CreateIndex(
                name: "IX_Authors_AdressEntityId",
                table: "Authors",
                column: "AdressEntityId",
                unique: true,
                filter: "[AdressEntityId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Authors_Adresses_AdressEntityId",
                table: "Authors",
                column: "AdressEntityId",
                principalTable: "Adresses",
                principalColumn: "AdressId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
