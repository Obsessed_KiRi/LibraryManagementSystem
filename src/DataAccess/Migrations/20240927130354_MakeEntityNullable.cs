﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class MakeEntityNullable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Authors_AdressEntityId",
                table: "Authors");

            migrationBuilder.DropIndex(
                name: "IX_Authors_ContactInfoEntityId",
                table: "Authors");

            migrationBuilder.AlterColumn<int>(
                name: "ContactInfoEntityId",
                table: "Authors",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "AdressEntityId",
                table: "Authors",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Authors_AdressEntityId",
                table: "Authors",
                column: "AdressEntityId",
                unique: true,
                filter: "[AdressEntityId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Authors_ContactInfoEntityId",
                table: "Authors",
                column: "ContactInfoEntityId",
                unique: true,
                filter: "[ContactInfoEntityId] IS NOT NULL");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Authors_AdressEntityId",
                table: "Authors");

            migrationBuilder.DropIndex(
                name: "IX_Authors_ContactInfoEntityId",
                table: "Authors");

            migrationBuilder.AlterColumn<int>(
                name: "ContactInfoEntityId",
                table: "Authors",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AdressEntityId",
                table: "Authors",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Authors_AdressEntityId",
                table: "Authors",
                column: "AdressEntityId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Authors_ContactInfoEntityId",
                table: "Authors",
                column: "ContactInfoEntityId",
                unique: true);
        }
    }
}
