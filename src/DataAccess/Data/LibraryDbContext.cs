﻿using Microsoft.EntityFrameworkCore;
using DataAccess.Entities;
using DataAccess.Сonfig;
using Spectre.Console;
using Microsoft.Extensions.Logging;

namespace DataAccess.Data;

public class LibraryDbContext : DbContext
{
    private readonly string _connectionString = @"Server=localhost;Database=LibrarySystem;Trusted_Connection=True;TrustServerCertificate=True;";
    public DbSet<Book> Books { get; set; } = null!;
    public DbSet<Author> Authors { get; set; } = null!;
    public DbSet<Address> Addresses { get; set; } = null!;
    public DbSet<ContactInfo> Contacts { get; set; } = null!;
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(_connectionString);
        optionsBuilder.LogTo(AnsiConsole.WriteLine, LogLevel.Information);
    }
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(LibraryDbContext).Assembly);
    }
}
