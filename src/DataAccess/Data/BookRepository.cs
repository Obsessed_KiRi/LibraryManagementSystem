﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessLogic.Interfaces;
using Spectre.Console;
using System.Linq;
using DataAccess.DTO;
using Microsoft.Win32.SafeHandles;

namespace DataAccess.Data;

public class BookRepository : IBookRepository
{
    private readonly LibraryDbContext _context;

    public BookRepository(LibraryDbContext context)
    {
        _context = context;
    }

    public async Task AddBookAsync(Book book)
    {
        await _context.Books.AddAsync(book);
        await _context.SaveChangesAsync();
    }

    public async Task UpdateBookAsync(Book book)
    {
        _context.Entry(book).State = EntityState.Modified;
        await _context.SaveChangesAsync();
    }

    public async Task DeleteBook(Book book)
    {
        _context.Books.Remove(book);
        await _context.SaveChangesAsync();
    }

    public async Task<Book> GetBookByIdAsync(int id)
    {
        var bookInfo = await _context.Books
            .Where(t => t.BookId == id)
            .Include(b => b.Author)
            .FirstOrDefaultAsync();
        return bookInfo;
    }

    public async Task<Book> GetBookByTitleAsync(string title)
    {
        var bookInfo = await _context.Books
            .Where(t => t.Title == title)
            .Include(b => b.Author)
            .FirstOrDefaultAsync();
        return bookInfo;
    }

    public async Task<List<Book>> GetAllBookInfosAsync()
    {
        var bookInfo = await _context.Books
        .Include(b => b.Author)
        .ToListAsync();
        return bookInfo;
    }

    public async Task<List<BookWithAuthorNameDTO>> GetBookByAuthorAsync(string firstName, string lastName)
    {
        var info = await _context.Books
            .Join(_context.Authors,
            books => books.AuthorNumber,
            author => author.AuthorId,
            (books, author) => new { books, author })
            .Where(a => a.author.FirstName == firstName && a.author.LastName == lastName)
            .Select(a => new BookWithAuthorNameDTO
            {
                Id = a.books.BookId,
                Title = a.books.Title,
                Year = a.books.PublishYear,
                AuthorName = $"{a.author.FirstName} {a.author.LastName}"
            })
            .ToListAsync();
        return info;
    }
    public async Task<BookCountDTO> GetAmountOfBooksAsync()
    {
        var count = await _context.Books
            .FromSqlRaw("SELECT COUNT(*) AS BookId FROM Books")
            .Select(b => new BookCountDTO { BookCount = b.BookId })
            .FirstOrDefaultAsync();

        return count;
    }
}