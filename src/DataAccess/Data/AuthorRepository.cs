﻿using DataAccess.Entities;
using DataAccess.Interfaces;
using Microsoft.EntityFrameworkCore;
using DataAccess.DTO;

namespace DataAccess.Data;

public class AuthorRepository : IAuthorRepository
{
    private readonly LibraryDbContext _context;
    public AuthorRepository(LibraryDbContext context)
    {
        _context = context;
    }

    public async Task UpdateAuthorInfoAsync(Author author)
    {
        _context.Entry(author).State = EntityState.Modified;
        await _context.SaveChangesAsync();
    }

    public async Task<Author> GetAuthorByIdAsync(int id)
    {
        var author = await _context.Authors
            .FirstOrDefaultAsync(a => a.AuthorId == id);
        return author;
    }

    public async Task<Author> GetAuthorByFullNameAsync(string firstName, string lastName)
    {
        var authorInfo = await _context.Authors
            .FirstOrDefaultAsync(a => a.FirstName == firstName && a.LastName == lastName);
        return authorInfo;
    }

    public async Task<List<AuthorBookCountDTO>> GetAuthorsAndBookNumbersAsync()
    {
        var info = await _context.Authors
           .Join(_context.Books,
           authors => authors.AuthorId,
           books => books.AuthorNumber,
            (author, book) => new
            {
                author.AuthorId,
                author.FirstName,
                author.LastName,
                book.BookId
            })
   .GroupBy(
       a => new { a.AuthorId, a.FirstName, a.LastName }
   )
   .Select(s => new AuthorBookCountDTO
   {
       AuthorName = s.Key.FirstName + " " + s.Key.LastName,
       BookCount = s.Count()
   })
   .ToListAsync();
        return info;
    }

    public async Task<List<AuthorWithBookYearDTO>> GetAuthorWithBookYearAsync(int year)
    {
        var info = await _context.Authors
                .Join(_context.Books,
                authors => authors.AuthorId,
                books => books.AuthorNumber,
                (author, book) => new { author, book })
                .Where(y => y.book.PublishYear == year)
                .Select(y => new AuthorWithBookYearDTO
                {
                    AuthorFullName = $"{y.author.FirstName} {y.author.LastName}",
                    PublishYear = y.book.PublishYear,
                })
                .ToListAsync();
        return info;
    }
    public async Task<List<AuthorWithAddressDTO>> GetAuthorByAdressAndCountry(string adress, string country)
    {
        var info = await _context.Authors
                .Join(_context.Addresses,
                authors => authors.AddressEntityId,
                adresses => adresses.AddressId,
                (authors, adresses) => new { authors, adresses })
                .Where(a => a.adresses.AddressName == adress && a.adresses.Country == country)
            .Select(a => new AuthorWithAddressDTO
            {
                AuthorFullName = $"{a.authors.FirstName} {a.authors.LastName}",
                FullAddress = $"{a.adresses.AddressName} {a.adresses.Country}",
            })
            .ToListAsync();
        return info;
    }

    public async Task<List<Author>> GetAuthorsByNameAsync(string firstName)
    {
        var authors = await _context.Authors
            .Where(a => a.FirstName == firstName)
            .ToListAsync();
        return authors;
    }
}