﻿
namespace DataAccess.DTO;
public class AuthorBookCountDTO
{
    public string AuthorName { get; set; }
    public int BookCount { get; set; }
}
