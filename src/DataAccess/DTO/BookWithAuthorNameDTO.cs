﻿namespace DataAccess.DTO;
public class BookWithAuthorNameDTO
{
    public int Id { get; set; }         
    public string Title { get; set; }   
    public int Year { get; set; }     
    public string AuthorName { get; set; } 
}