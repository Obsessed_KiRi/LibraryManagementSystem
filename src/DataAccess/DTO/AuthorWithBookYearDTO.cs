﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.DTO;

public class AuthorWithBookYearDTO
{
    public string AuthorFullName { get; set; }
    public int PublishYear { get; set; }
}
