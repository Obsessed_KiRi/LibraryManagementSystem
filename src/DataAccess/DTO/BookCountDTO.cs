﻿namespace DataAccess.DTO;

public class BookCountDTO
{
    public int BookCount { get; set; }
}
