﻿namespace DataAccess.DTO;

public class AuthorWithAddressDTO
{
    public string AuthorFullName { get; set; }
    public string FullAddress { get; set; }
}
