﻿
using DataAccess.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Config;

public class AddressConfig : IEntityTypeConfiguration<Address>
{
    public void Configure(EntityTypeBuilder<Address> builder)
    {
        builder
           .ToTable("Adresses");
        builder.HasKey(b => b.AddressId);

        builder.HasOne(b => b.Author)
              .WithOne(b => b.Address)
              .HasForeignKey<Author>(b => b.AddressEntityId)
         .OnDelete(DeleteBehavior.Cascade);
    }
}