﻿using DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccess.Сonfig;

public class BookConfig : IEntityTypeConfiguration<Book>
{
    public void Configure(EntityTypeBuilder<Book> builder)
    {
        builder
           .ToTable("Books");
        builder.HasKey(b => b.BookId);

        builder.HasOne(b => b.Author)
              .WithMany(b => b.Books)
              .HasForeignKey(b => b.AuthorNumber)
              .OnDelete(DeleteBehavior.Cascade);
    }
}
