﻿
using DataAccess.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Config;

public class ContactInfoConfig : IEntityTypeConfiguration<ContactInfo>
{
    public void Configure(EntityTypeBuilder<ContactInfo> builder)
    {
        builder
           .ToTable("Contacts");
        builder.HasKey(b => b.ContactInfoId);

        builder.HasOne(b => b.Author)
              .WithOne(b => b.ContactInfo)
              .HasForeignKey<Author>(b => b.ContactInfoEntityId)
         .OnDelete(DeleteBehavior.Cascade);
    }
}