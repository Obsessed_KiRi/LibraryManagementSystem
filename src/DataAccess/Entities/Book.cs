﻿namespace DataAccess.Entities;

public class Book
{
    public int BookId { get; set; }
    public string Title { get; set; }
    public int PublishYear { get; set; }
    public string Genre { get; set; }

    public int AuthorNumber { get; set; }
    public Author Author { get; set; } = null!;
}