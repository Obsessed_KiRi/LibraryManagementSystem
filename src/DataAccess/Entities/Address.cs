﻿namespace DataAccess.Entities;

public class Address
{
    public int AddressId { get; set; }
    public string AddressName { get; set; }
    public string Country { get; set; }

    public Author Author { get; set; }
}