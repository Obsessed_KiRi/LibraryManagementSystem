﻿
namespace DataAccess.Entities;

public class ContactInfo
{
    public int ContactInfoId { get; set; }
    public string PhoneNumber { get; set; }
    public string Mail { get; set; }

    public Author Author { get; set; }
}
