﻿namespace DataAccess.Entities;
public class Author
{
    public int AuthorId { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }

    public ICollection<Book> Books { get; set; } = [];

    public int? AddressEntityId { get; set; }
    public Address? Address { get; set; }

    public int? ContactInfoEntityId { get; set; }
    public ContactInfo? ContactInfo { get; set; }
}